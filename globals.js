import {StyleSheet} from "react-native";

const globalStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFE2B7',
    },
    button: {
    backgroundColor: 'transparent',
        height: 40,
        borderColor: '#F4A900',
        borderWidth: 2,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
        margin: 5,
},
buttonText: {
    color: '#F4A900',
        fontWeight: 'bold',
        fontSize: 16
}
});

export default globalStyles;
