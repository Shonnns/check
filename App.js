import { StatusBar } from 'expo-status-bar';
import {Button, Pressable, StyleSheet, Text, View} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MainScreen from "./pages/Main";
import {SafeAreaView} from "react-native-safe-area-context";
import EditCheck from "./pages/EditCheck";
import AddCheck from "./pages/AddCheck";
import {Provider} from "react-redux";
import {store} from "./store";
import AddFriend from "./pages/addFriend";
import AddPosition from "./pages/addPosition";
import ScannerScreen from "./pages/scanner";
const Stack = createNativeStackNavigator();


export default function App() {


  return (
      <Provider store={store}>
          <NavigationContainer>
              <Stack.Navigator
                  screenOptions={{
                      headerStyle: {
                          backgroundColor: '#FFCF48',
                      }
                  }}>
                  <Stack.Group>
                      <Stack.Screen name="Main" component={MainScreen} options={({navigation}) => ({
                          title: 'Мои чеки',
                          headerRight: () => (<Pressable style={{height: 20, width: 20}} onPress={() => navigation.navigate('NewCheck')}>
                              <Text style={{fontSize: 16}}>+</Text>
                          </Pressable>)
                      })}/>
                      <Stack.Screen name="Edit" component={EditCheck}
                                    options={
                                        ({ route, navigation}) =>({
                                            title: route?.params?.title,
                                            headerRight: () => (<Pressable style={{height: 20, width: 20}} onPress={() => navigation.navigate('Scanner')}>
                                                <Text style={{fontSize: 16}}>📷</Text>
                                            </Pressable>)
                                        })} />
                  </Stack.Group>
                  <Stack.Group screenOptions={{ presentation: 'modal'}}>
                      <Stack.Screen name="NewCheck" component={AddCheck} options={{title: 'Новый чек'}}/>
                      <Stack.Screen name="newFriend" component={AddFriend} options={{title: 'Добавить друга'}}/>
                      <Stack.Screen name="newPosition" component={AddPosition} options={{title: 'Добавить позицию'}}/>
                      <Stack.Screen name="Scanner" component={ScannerScreen} options={{title: 'Сканировать чек'}}/>
                  </Stack.Group>
              </Stack.Navigator>
          </NavigationContainer>
      </Provider>
  );
}
