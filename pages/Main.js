import {FlatList, Pressable, StyleSheet, Text, View} from "react-native";
import globalStyles from "../globals";
import { ref, onValue, set } from "firebase/database";
import database from "../config";
import {useEffect, useState} from "react";

import AsyncStorage from "@react-native-async-storage/async-storage";
import {useDispatch, useSelector} from "react-redux";
import {setFriend} from "../reducers/friend";
import {setCheck} from "../reducers/check";
import {setCheckStatic} from "../reducers/check_static";

const loadChecks = (setChecks) => {
    const {check_static} = useSelector(state => state.check_static);
    const dispatch = useDispatch();

    useEffect(() => {
        const getChecks = () => {
            onValue(ref(database, `/checks`), (value) => {
                if (value.val()) {
                    if(check_static?.id) {
                        dispatch(setCheck(value.val()[check_static.id]));

                    }

                    setChecks(Object.values(value.val()))
                }
            })
        }

        getChecks();
    }, []);
}

const MainScreen = ({navigation}) => {
    const [checks, setChecks] = useState([])

    loadChecks(setChecks);

    const removeCheck = (id) => {
        set(ref(database, 'checks/' + id), {
        }).then(() => {
            loadChecks(setChecks);
        })
    }
    const dispatch = useDispatch();


    const RenderItem = ({item})=> {
        return(
            <Pressable style={styles.item}
                        onLongPress={() => {
                            removeCheck(item?.id);
                        }}
                       onPress={()=> {
                           dispatch(setCheckStatic(item));
                           dispatch(setCheck(item));
                           dispatch(setFriend({}));

                           navigation.navigate('Edit', {title: item?.name});
                       }
            }>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{flex: 1}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>{item?.name}</Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <View style={{padding: 5, alignItems: 'flex-end'}}>
                            <Text style={{color: 'gray'}}>{[new Date(item?.date).toLocaleDateString(), ' 📅']}</Text>
                            <Text style={{marginTop: 5}}>{[Object.values(item?.positions || []).reduce((reducer,i) => reducer + Number(i.cost), 0), '₽']}</Text>
                        </View>

                    </View>
                </View>
            </Pressable>
        )
    }

    return (
        <View style={[globalStyles.container, { padding: 10 }]}>
            <FlatList data={checks} renderItem={({item})=> <RenderItem item={item}/>}/>
        </View>
    )
}

export default MainScreen

const styles = StyleSheet.create({
    item: {
        flex: 1,
        margin: 5,
        backgroundColor: '#fff',
        padding: 10,
        borderRadius: 10
    },
});