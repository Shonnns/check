import {Text, TextInput, View, StyleSheet, Pressable} from "react-native";
import globalStyles from "../globals";
import {useEffect, useState} from "react";
import DatePicker from "expo-datepicker";
import {ref, set} from "firebase/database";
import database from "../config";

const AddCheck = ({navigation, route}) => {
    const [name, setName] = useState('');
    const [date, setDate] = useState(new Date().toString());

    const id = new Date().getTime().toString();

    const createCheck = () => {
        set(ref(database, 'checks/' + id), {
            id: id,
            name: name,
            date: date,
            friends: {},
            positions: {}
        }).then(() => {
            navigation.goBack()
            navigation.navigate('Edit', { id: id, title: name })
        })
    }

    return (
        <View style={[globalStyles.container]}>
            <TextInput
                style={styles.input}
                value={name}
                onChangeText={setName}
                placeholder='Название'
            >
            </TextInput>
            <View style={styles.dateBox}>
                <Text style={{color: 'gray'}}>Дата</Text>
                <DatePicker
                    date={date}
                    onChange={(date) => setDate(date)}
                />
            </View>
            <View style={{flex: 1, justifyContent: 'flex-end', paddingBottom: 50}}>
                <Pressable style={globalStyles.button} onPress={() => {
                    createCheck();
                }}>
                    <Text style={globalStyles.buttonText}>Создать</Text>
                </Pressable>
            </View>
        </View>
    )
}

export default AddCheck;

const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 5,
        backgroundColor: '#FFDB8B',
        borderRadius: 10,
        padding: 10,
    },
    dateBox: {
        backgroundColor: '#FFDB8B',
        margin: 5,
        borderRadius: 10,
        padding: 10
    }
});