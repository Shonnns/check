import {FlatList, Pressable, Text, View} from "react-native";
import globalStyles from "../globals";
import {useDispatch, useSelector} from "react-redux";
import {onValue, ref, set} from "firebase/database";
import database from "../config";
import {setFriend} from "../reducers/friend";
import {useEffect, useState} from "react";

const PositionsScreen = ({ route, navigation }) => {
    const [positions, setPositions] = useState([]);
    const {check_static} = useSelector(state => state.check_static);
    const {check} = useSelector(state => state.check);
    const {friend} = useSelector(state => state.friend);

    const removePositions = (id) => {
        set(ref(database, `/checks/${check_static?.id}/positions/${id}`), {})
            .catch((err)=>console.log(err))
    }

    const setPositionFriend = (id) => {
        set(ref(database, `/checks/${check_static?.id}/positions/${id}/friend_id`), friend.id)
            .catch((err)=>console.log(err))
    }

    useEffect(() => {
        const getFriends = () => {
            onValue(ref(database, `/checks/${check_static?.id}/positions`), (value) => {
                if (value.val()) {
                    setPositions(Object.values(value.val()));
                }
            })
        }

        getFriends();
    }, [check_static]);

    const PositionItem = ({item}) => {
        const dispatch = useDispatch();
        return (
            <Pressable style={{
                flexDirection: 'row',
                backgroundColor: item?.friend_id ? check?.friends[item?.friend_id]?.color : '#fff',
                height: 60,
                margin: 5,
                padding: 10,
                justifyContent: 'center',
                borderRadius: 10
            }}
                       onLongPress={() => {
                           removePositions(item.id);
                       }}
                       onPress={()=> {
                           setPositionFriend(item.id);
                       }}>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>{item?.name}</Text>
                </View>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-end'}}>
                    <Text style={{fontSize: 16, color: 'gray' }}>{[item?.cost || 0, '₽']}</Text>
                </View>
            </Pressable>
        )
    }

    return (
        <View style={globalStyles.container}>
            <Pressable style={globalStyles.button} onPress={() => {
                navigation.navigate('newPosition');
            }}>
                <Text style={globalStyles.buttonText}>Добавить позицию</Text>
            </Pressable>
            <FlatList data={positions} renderItem={({item})=> <PositionItem item={item}/>}/>
        </View>
    )
}

export default PositionsScreen;