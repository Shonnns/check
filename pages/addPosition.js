import {Text, TextInput, View, StyleSheet, Pressable, Keyboard} from "react-native";
import globalStyles from "../globals";
import {useState} from "react";
import {ref, set} from "firebase/database";
import database from "../config";
import {useSelector} from "react-redux";

const AddPosition = ({navigation, route}) => {
    const [name, setName] = useState('');
    const [cost, setCost] = useState('');

    const {check_static} = useSelector(state => state.check_static);

    const id = new Date().getTime().toString();

    const addPosition = () => {
        set(ref(database, `checks/${check_static?.id}/positions/${id}` ), {
            id: id,
            name: name,
            cost: Number(cost)
        }).then(() => {
            navigation.goBack();
        })
    }

    return (
        <Pressable style={[globalStyles.container]} onPress={()=> {
            console.log('das');
            Keyboard.dismiss
        }}>
            <TextInput
                style={styles.input}
                value={name}
                onChangeText={setName}
                placeholder='Название'
            >
            </TextInput>
            <TextInput
                style={styles.input}
                value={cost}
                onChangeText={setCost}
                placeholder='Цена'
                keyboardType="numeric"
            >
            </TextInput>
            <View style={{flex: 1, justifyContent: 'flex-end', paddingBottom: 50}}>
                <Pressable style={globalStyles.button} onPress={() => {
                    addPosition();
                }}>
                    <Text style={globalStyles.buttonText}>Добавить</Text>
                </Pressable>
            </View>
        </Pressable>
    )
}

export default AddPosition;

const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 5,
        backgroundColor: '#FFDB8B',
        borderRadius: 10,
        padding: 10,
    }
});