import {Pressable, Text, View} from "react-native";
import {useEffect, useState} from "react";
import {onValue, ref} from "firebase/database";
import database from "../config";
import globalStyles from "../globals";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {NavigationContainer} from "@react-navigation/native";
import PositionsScreen from "./Positions";
import FriendsScreen from "./Friends";
import {useSelector} from "react-redux";
import AddCheck from "./AddCheck";
import AddFriend from "./addFriend";
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();

const EditCheck = ({navigation, route}) => {
    const {friend} = useSelector(state => state.friend);
    const {check} = useSelector(state => state.check);

    return (
        <View style={{flex: 1, backgroundColor: '#FFE2B7'}}>
            { friend?.name ? <View style={{
                flexDirection: 'row',
                backgroundColor: friend?.color,
                height: 60,
                margin: 5,
                padding: 10,
                justifyContent: 'center',
                borderRadius: 10
            }}>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>{friend?.name}</Text>
                </View>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-end'}}>
                    <Text style={{fontSize: 16, color: 'gray' }}>{[Object.values(check.positions)
                        .filter(pr => pr.friend_id === friend?.id)
                        .reduce((reducer, i) => reducer + Number(i.cost), 0)|| 0, '₽']}</Text>
                </View>
            </View> : null }

            <Tab.Navigator screenOptions={{
                headerShown: false,
                tabBarActiveTintColor: '#F4A900',
                tabBarInactiveTintColor: '#FFE2B7',
                tabBarStyle: {
                    backgroundColor: '#FFCF48',
                },
            }}>
                <Tab.Screen name="Positions" component={PositionsScreen} options={{title: 'Позиции'}} />
                <Tab.Screen name="Friends" component={FriendsScreen} options={{title: 'Друзья'}} />
            </Tab.Navigator>
        </View>
    )
}

export default EditCheck