import {FlatList, Pressable, Text, View} from "react-native";
import globalStyles from "../globals";
import randomColor from 'randomcolor';
import {onValue, ref, set} from "firebase/database";
import database from "../config";
import {useEffect, useState} from "react";
import asyncStorage from "@react-native-async-storage/async-storage/src/AsyncStorage";
import friend, {setFriend} from "../reducers/friend";
import {useDispatch, useSelector} from "react-redux";


const FriendsScreen = ({ route, navigation }) => {
    const [friends, setFriends] = useState([]);
    const {check_static} = useSelector(state => state.check_static);
    const {check} = useSelector(state => state.check);

    const removeFriends = (id) => {
        set(ref(database, `/checks/${check_static.id}/friends/${id}`), {})
    }

    useEffect(() => {
        const getFriends = () => {
            onValue(ref(database, `/checks/${check_static.id}/friends`), (value) => {
                if (value.val()) {
                    setFriends(Object.values(value.val()))
                }
            })
        }

        getFriends();
    }, [check_static]);

    const FriendItem = ({item}) => {
        const dispatch = useDispatch();
        return (
            <Pressable style={{
                flexDirection: 'row',
                backgroundColor: item?.color,
                height: 60,
                margin: 5,
                padding: 10,
                justifyContent: 'center',
                borderRadius: 10
            }}
                       onLongPress={() => {
                           removeFriends(item.id)
                       }}
                       onPress={()=> {
                           dispatch(setFriend(item));
                       }}>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>{item?.name}</Text>
                </View>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-end'}}>
                    <Text style={{fontSize: 16, color: 'gray' }}>{[
                        Object.values(check.positions)
                            .filter(pr => pr.friend_id === item.id)
                            .reduce((reducer, i) => reducer + Number(i.cost), 0)
                        || 0, '₽']}</Text>
                </View>
            </Pressable>
        )
    }

    return (
        <View style={globalStyles.container}>
            <Pressable style={globalStyles.button} onPress={() => {
                navigation.navigate('newFriend');
            }}>
                <Text style={globalStyles.buttonText}>Добавить друга</Text>
            </Pressable>
            <FlatList data={friends} renderItem={({item})=> <FriendItem item={item}/>}/>
        </View>
    )
}


export default FriendsScreen;