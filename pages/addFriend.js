import {Text, TextInput, View, StyleSheet, Pressable} from "react-native";
import globalStyles from "../globals";
import {useState} from "react";
import {ref, set} from "firebase/database";
import database from "../config";
import {useSelector} from "react-redux";
import randomColor from "randomcolor";

const AddCheck = ({navigation, route}) => {
    const [name, setName] = useState('');

    const {check_static} = useSelector(state => state.check_static);

    const id = new Date().getTime().toString();

    const addFriend = () => {
        set(ref(database, `checks/${check_static?.id}/friends/${id}` ), {
            id: id,
            name: name,
            color: randomColor({luminosity: 'light'}),
            sum: 0
        }).then(() => {
            navigation.goBack();
        })
    }

    return (
        <View style={[globalStyles.container]}>
            <TextInput
                style={styles.input}
                value={name}
                onChangeText={setName}
                placeholder='Имя'
            >
            </TextInput>
            <View style={{flex: 1, justifyContent: 'flex-end', paddingBottom: 50}}>
                <Pressable style={globalStyles.button} onPress={() => {
                    addFriend();
                }}>
                    <Text style={globalStyles.buttonText}>Добавить</Text>
                </Pressable>
            </View>
        </View>
    )
}

export default AddCheck;

const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 5,
        backgroundColor: '#FFDB8B',
        borderRadius: 10,
        padding: 10,
    }
});