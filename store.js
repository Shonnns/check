import { configureStore } from '@reduxjs/toolkit';
import friendSlice from './reducers/friend';
import checkSlice from './reducers/check';
import checkStaticSlice from './reducers/check_static';

export const store = configureStore({
    reducer: {
        friend: friendSlice,
        check: checkSlice,
        check_static: checkStaticSlice,
    }
});