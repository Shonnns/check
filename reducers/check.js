import {createAsyncThunk} from "@reduxjs/toolkit";

import { createSlice } from "@reduxjs/toolkit"

const checkSlice = createSlice({
    name: "check",
    initialState: {
        check: {}
    },
    reducers: {
        setCheck(state, action) {
            state.check = action.payload
        }
    }
})

export const { setCheck } = checkSlice.actions
export default checkSlice.reducer;