import {createAsyncThunk} from "@reduxjs/toolkit";

import { createSlice } from "@reduxjs/toolkit"

const checkStaticSlice = createSlice({
    name: "check_static",
    initialState: {
        check_static: {}
    },
    reducers: {
        setCheckStatic(state, action) {
            state.check_static = action.payload
        }
    }
})

export const { setCheckStatic } = checkStaticSlice.actions
export default checkStaticSlice.reducer;