import { createSlice, PayloadAction } from "@reduxjs/toolkit"

const friendSlice = createSlice({
    name: "friend",
    initialState: {
        friend: {}
    },
    reducers: {
        setFriend(state, action) {
            state.friend = action.payload
        }
    }
})

export const { setFriend } = friendSlice.actions
export default friendSlice.reducer;